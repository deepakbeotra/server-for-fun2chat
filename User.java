/**
 * Model Class  to store user details
 * 
 * @author deepak
 *
 */
public class User {

	private String userName;
	private String ipAddress;

	private String firstName;
	private String lastName;
	private String password;
	private String contactNumber;
	private String sex;
	
	
	public User(String userName, String contactNumber, String firstName,
			String lastName, String sex, String password, String ipAddress) {
		this.userName = userName;
		this.contactNumber = contactNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.sex = sex;
		this.password = password;
		this.ipAddress = ipAddress;
		
	}
	
	
	public String getUserName() {
		return userName;
	}
	
	

	public String getContactNumber() {
		return contactNumber;
	}
	public String getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
